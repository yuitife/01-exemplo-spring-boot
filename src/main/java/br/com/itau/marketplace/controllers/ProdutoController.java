package br.com.itau.marketplace.controllers;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import br.com.itau.marketplace.models.Produto;
import br.com.itau.marketplace.repositories.ProdutoRepository;

@RestController
@RequestMapping("/produto")
public class ProdutoController {
	@Autowired
	ProdutoRepository produtoRepository;

	@GetMapping
	public Iterable<Produto> listarProdutos() {
		return produtoRepository.findAll();
	}

	@GetMapping("/{id}")
	public Optional<Produto> listarProdutoPorId(@PathVariable int id) {
		return produtoRepository.findById(id);
	}

	@PostMapping
	public void inserirProduto(@RequestBody Produto produto) {
		produtoRepository.save(produto);
	}
}
